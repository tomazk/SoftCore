﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ElegantDI.Composition
{
    public interface IPartSatisfiedCallback
    {
        void OnImportsSatisfied();
    }
}
