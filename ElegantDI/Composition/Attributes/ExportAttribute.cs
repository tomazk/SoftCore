﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ElegantDI.Composition
{
    /// <summary>
    /// The class with this attribute will be put into the IoC container.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = false)]
    public sealed class ExportAttribute : Attribute
    {
        public ExportLifetime ExportLifetime { get; } = ExportLifetime.Singleton;

        public ExportAttribute(ExportLifetime exportLifetime = ExportLifetime.Singleton)
        {
            Contracts = Array.Empty<Contract>();
            ExportLifetime = exportLifetime;
        }
        public ExportAttribute(Type[] contracts, ExportLifetime exportLifetime)
        {
            Contracts = contracts
                      .Select(x => CompositionTools.GetContractFromType(x))
                      .ToArray();
            ExportLifetime = exportLifetime;
        }
        public ExportAttribute(params Type[] contracts)
        {
            Contracts = contracts
                .Select(x => CompositionTools.GetContractFromType(x))
                .ToArray();
        }

        public IEnumerable<Contract> Contracts { get; internal set; }
    }
}
