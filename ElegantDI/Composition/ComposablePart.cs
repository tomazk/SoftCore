﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Linq;
using ElegantDI.Composition.LifetimeManagement;

namespace ElegantDI.Composition
{
    /// <summary>
    /// Represents the part in the IoC container that has at least one export and can have imports.
    /// </summary>
    public class ComposablePart
    {
        public ComposablePart(Type partType, IEnumerable<ComposablePartExport> exports,
            IEnumerable<ComposablePartImport> imports, LifetimeManager lifetimeManager)
        {
            this.PartType = partType;
            this.Exports = exports;
            this.Imports = imports;
            this.LifetimeManager = lifetimeManager;
        }

        public ComposablePart(Type partType)
        {
            // TODO: although Part itself doesn't know anything about attributes for export, improt and lifetime,
            // the attributes are used here only to construct it. It would be wise to move this code out of
            // this class.

            this.PartType = partType;

            ComposabePartInfo composabePartInfo = CompositionTools.GetComposablePartInfo(partType);

            this.Imports = composabePartInfo.Imports;
            this.Exports = composabePartInfo.Exports;

            // Create lifetime manager
            if (composabePartInfo.Lifetime == ExportLifetime.Singleton)
                LifetimeManager = new SharedLifetimeManager(partType);
            else if (composabePartInfo.Lifetime == ExportLifetime.Transient)
                LifetimeManager = new TransientLifetimeManager(partType);
            else if (composabePartInfo.Lifetime == ExportLifetime.Scoped)
                LifetimeManager = new ScopeLifetimeManager(PartType);
            else
                throw new Exception($"Lifetime '{composabePartInfo.Lifetime}' not supported");
        }
        public ComposablePart(Type partType, LifetimeManager lifetimeManager)
        {
            // TODO: although Part itself doesn't know anything about attributes for export, improt and lifetime,
            // the attributes are used here only to construct it. It would be wise to move this code out of
            // this class.

            this.PartType = partType;
            ComposabePartInfo composabePartInfo = CompositionTools.GetComposablePartInfo(partType);
            this.Imports = composabePartInfo.Imports;
            this.Exports = composabePartInfo.Exports;
            LifetimeManager = lifetimeManager;
        }

        public LifetimeManager LifetimeManager { get; private set; }

        public Type PartType { get; private set; }
        public IEnumerable<ComposablePartExport> Exports { get; private set; }
        public IEnumerable<ComposablePartImport> Imports { get; private set; }
    }
}
