﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ElegantDI.Licensing
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
    public class ImportLicenceParametersAttribute : Attribute
    {
    }
}
