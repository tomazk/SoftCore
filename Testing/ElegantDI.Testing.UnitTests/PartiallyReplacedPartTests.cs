﻿using NUnit.Framework;
using ElegantDI.Composition;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElegantDI.Testing.UnitTests
{
    public class PartiallyReplacedPartTests
    {
        [Test]
        public void TestPartiallyReplacedPart()
        {
            TypeCatalog catalog = new TypeCatalog(
                typeof(RealExport),
                typeof(TestClass));
            ReplacementTypeCatalog replacementCatalog = new ReplacementTypeCatalog(catalog,
                typeof(ReplacementExport));

            ElegantContainer container = new ElegantContainer(replacementCatalog);

            Root root = new Root();
            container.Bootstrap(root);
            Assert.IsTrue(root.TestClass?.TestExport1 is ReplacementExport);
            Assert.IsTrue(root.TestClass?.TestExport2 is RealExport);
        }

        class Root
        {
            [Import]
            private TestClass? testClass = null;

            public TestClass? TestClass => testClass;
        }

        #region Classes
        interface ITestExport1
        {
        }
        interface ITestExport2
        {
        }

        [Export(typeof(ITestExport1), typeof(ITestExport2))]
        class RealExport : ITestExport1, ITestExport2
        {
            private RealExport()
            {
            }
        }
        [ReplacementExport(typeof(ITestExport1))]
        class ReplacementExport : ITestExport1
        {
            private ReplacementExport()
            {
            }
        }

        [Export]
        class TestClass
        {
            [Import(typeof(ITestExport1))]
            private ITestExport1? testExport1 = null;
            [Import(typeof(ITestExport2))]
            private ITestExport2? testExport2 = null;

            private TestClass()
            {
            }

            public ITestExport1? TestExport1 => testExport1;
            public ITestExport2? TestExport2 => testExport2;
        }
        #endregion
    }
}
