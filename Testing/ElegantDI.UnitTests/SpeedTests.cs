﻿using NUnit.Framework;
using ElegantDI.Composition;
using System.Diagnostics;

namespace ElegantDI.UnitTests
{
    public class SpeedTests
    {
        [Test]
        public void SpeedTest()
        {
            TypeCatalog catalog = new TypeCatalog(
                    typeof(ClassB),
                    typeof(ClassC),
                    typeof(ClassD),
                    typeof(ClassE),
                    typeof(ClassA));

            Stopwatch clock = new Stopwatch();
            clock.Start();

            for (int i = 0; i < 1000; i++)
            {
                ElegantContainer container = new ElegantContainer(catalog);
                Root root = new Root();
                container.Bootstrap(root);
            }

            clock.Stop();
            Assert.Pass("Time: " + clock.ElapsedMilliseconds.ToString("N0") + "ms");
        }
    }

    class Root
    {
        [Import] private ClassA? item = null;

        public ClassA? Item => item;
    }

    #region Classes
    [Export(ExportLifetime.Transient)]
    public class ClassA
    {
        [Import]
        private ClassB? classB = null;

        [Import(typeof(ClassC))]
        private ClassC? classC = null;

        [Import]
        private ClassD? classD = null;

        private ClassA()
        {
        }

        public ClassB? ClassB => classB;
        public ClassC? ClassC => classC;
        public ClassD? ClassD => classD;
    }

    [Export(ExportLifetime.Transient)]
    public class ClassB
    {
        private ClassB()
        {
        }
    }
    [Export(new System.Type[] { typeof(ClassC) }, ExportLifetime.Transient)]
    public class ClassC
    {
        private ClassC()
        {
        }
    }
    [Export(ExportLifetime.Transient)]
    public class ClassD
    {
        [Import]
        private ClassE? classE = null;

        private ClassD()
        {
        }

        public ClassE? ClassE => classE;
    }
    [Export(ExportLifetime.Transient)]
    public class ClassE
    {
        private ClassE()
        {
        }
    }
    #endregion
}
