﻿using NUnit.Framework;
using ElegantDI.Composition;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElegantDI.UnitTests.CompositionTests
{
    public class FloatingExportTests
    {
        [Test]
        public void TestFloatingExport()
        {
            TypeCatalog catalog = new TypeCatalog(
                typeof(ExportA),
                typeof(ExportB));

            ElegantContainer container = new ElegantContainer(catalog);
            Root root = new Root();
            container.Bootstrap(root);

            Assert.AreEqual(root.Item?.Value, 1234);
        }

        class Root
        {
            [Import]
            private ExportA? item = null;

            public ExportA? Item => item;
        }

        #region Classes
        [Export]
        [FloatingExport]
        public class ExportB : IPartSatisfiedCallback
        {
            [Import]
            private ExportA? importA = null;

            private ExportB()
            {
            }

            void IPartSatisfiedCallback.OnImportsSatisfied()
            {
                importA?.SetValue(1234);
            }
        }

        [Export]
        public class ExportA
        {
            public void SetValue(int value)
            {
                this.Value = value;
            }

            private ExportA()
            {
            }

            public int Value { get; private set; }
        }
        #endregion
    }
}
