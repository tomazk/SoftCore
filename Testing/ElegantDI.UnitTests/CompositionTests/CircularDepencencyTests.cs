﻿using NUnit.Framework;
using ElegantDI.Composition;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElegantDI.UnitTests.CompositionTests
{
    public class CircularDepencencyTests
    {
        [Import] ClassA? classA = null;

        [Test]
        public void TestCircularDependency()
        {
            TypeCatalog catalog = new TypeCatalog(
                typeof(ClassA),
                typeof(ClassB),
                typeof(ClassC));

            Assert.Catch(() =>
            {
                ElegantContainer container = new ElegantContainer(catalog);
                container.Bootstrap(this);

                if (classA?.ImportedClass?.ImportedClass?.ImportedClass != null)
                    throw new Exception("It should never get to this point. This was obviously executed in the twilight zone.");
            });
        }
    }

    [Export]
    public class ClassA
    {
        [Import]
        private ClassB? importedClass = null;

        public ClassB? ImportedClass => importedClass;

        private ClassA()
        {
        }
    }
    [Export]
    public class ClassB
    {
        [Import]
        private ClassC? importedClass = null;

        public ClassC? ImportedClass => importedClass;

        private ClassB()
        {
        }
    }
    [Export]
    public class ClassC
    {
        [Import]
        private ClassA? importedClass = null;

        public ClassA? ImportedClass => importedClass;

        private ClassC()
        {
        }
    }
}
