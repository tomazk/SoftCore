﻿using NUnit.Framework;
using ElegantDI.Composition;
using System;
using System.Collections.Generic;
using System.Text;

namespace ElegantDI.UnitTests.CompositionTests
{
    public class ExportFactoryTests
    {
        [Test]
        public void TestExportFactoryWithConstructorParameters()
        {
            TypeCatalog catalog = new TypeCatalog(
                typeof(ExportA),
                typeof(ExportB),
                typeof(ExportC),
                typeof(FactoryImporter));

            ElegantContainer container = new ElegantContainer(catalog);
            Root root = new Root();
            container.Bootstrap(root);

            ExportA? exportA = root.FactoryImporter?.ExportAFactory?.CreateExport();
            ExportB? exportB = root.FactoryImporter?.ExportBFactory?.CreateExport("Mike");
            ExportC? exportC = root.FactoryImporter?.ExportCFactory?.CreateExport("Veronique", 27);

            Assert.IsNotNull(exportA);
            Assert.IsNotNull(exportB);
            Assert.IsNotNull(exportC);
            Assert.AreEqual(exportB?.Name, "Mike");
            Assert.AreEqual(exportC?.Name, "Veronique");
            Assert.AreEqual(exportC?.Age, 27);
        }

        class Root
        {
            [Import]
            private FactoryImporter? factoryImporter = null;

            public FactoryImporter? FactoryImporter => factoryImporter;
        }

        [Test]
        public void TestIfNumberOfConstructorParameterCountMatch()
        {
            TypeCatalog catalog = new TypeCatalog(
                typeof(ExportC),
                typeof(FactoryImporterWithInvalidParameterCount));

            Assert.Catch(new TestDelegate(() =>
            {
                ElegantContainer container = new ElegantContainer(catalog);
            }));
        }
        [Test]
        public void TestIfConstructorParameterTypeMatches()
        {
            TypeCatalog catalog = new TypeCatalog(
                typeof(ExportC),
                typeof(FactoryImporterWithInvalidParameterType));

            Assert.Catch(new TestDelegate(() =>
            {
                ElegantContainer container = new ElegantContainer(catalog);
            }));
        }
        [Test]
        public void TestThatExportFactoryIsUsedWithConstructorParameters()
        {
            TypeCatalog catalog = new TypeCatalog(
                typeof(ExportC),
                typeof(FactoryImporterWithMissingExportFactory));

            Assert.Catch(new TestDelegate(() =>
            {
                ElegantContainer container = new ElegantContainer(catalog);
            }));
        }

        #region Classes
        [Export(ExportLifetime.Transient)]
        public class ExportA
        {
            private ExportA()
            {
            }
        }
        [Export(ExportLifetime.Transient)]
        public class ExportB
        {
            private ExportB(string name)
            {
                this.Name = name;
            }

            public string Name { get; private set; }
        }
        [Export(ExportLifetime.Transient)]
        public class ExportC
        {
            private ExportC(string name, int age)
            {
                this.Name = name;
                this.Age = age;
            }

            public string Name { get; private set; }
            public int Age { get; private set; }
        }
        [Export]
        public class FactoryImporter
        {
            [Import]
            private ExportFactory<ExportA>? exportAFactory = null;
            [Import]
            private ExportFactory<ExportB, string>? exportBFactory = null;
            [Import]
            private ExportFactory<ExportC, string, int>? exportCFactory = null;

            private FactoryImporter()
            {
            }

            public ExportFactory<ExportA>? ExportAFactory => exportAFactory;
            public ExportFactory<ExportB, string>? ExportBFactory => exportBFactory;
            public ExportFactory<ExportC, string, int>? ExportCFactory => exportCFactory;
        }
        [Export]
        public class FactoryImporterWithInvalidParameterCount
        {
            [Import]
            private ExportFactory<ExportC, string>? exportCFactory = null;

            private FactoryImporterWithInvalidParameterCount()
            {
            }

            public ExportFactory<ExportC, string>? ExportCFactory => exportCFactory;
        }
        [Export]
        public class FactoryImporterWithInvalidParameterType
        {
            [Import]
            private ExportFactory<ExportC, string, string>? exportCFactory = null;

            private FactoryImporterWithInvalidParameterType()
            {
            }

            public ExportFactory<ExportC, string, string>? ExportCFactory => exportCFactory;
        }
        [Export]
        public class FactoryImporterWithMissingExportFactory
        {
            [Import]
            private ExportC? exportC = null;

            private FactoryImporterWithMissingExportFactory()
            {
            }

            public ExportC? ExportC => exportC;
        }
        #endregion
    }
}
